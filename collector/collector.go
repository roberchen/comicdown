/*Package collector will collect all mangas info
 */
package collector

import (
	"encoding/json"
	"io/ioutil"
	"os"
	"sync"
	"time"
)

// Collector stores all collected mangas' informations
type Collector struct {
	Indexer map[string]*Manga `json:"index"`
	Loc     sync.Mutex        `json:"-"`
	Wait    sync.WaitGroup    `json:"-"`
}

// NewCollector generate new collector
func NewCollector() *Collector {
	return &Collector{
		Indexer: make(map[string]*Manga),
	}
}

// MangaStatus will store the Status of a manga
type MangaStatus string

// MangaSource is source of manga
type MangaSource string

const (
	// Dmzj manga source tag
	Dmzj MangaSource = "dmzj"
)

// Manga stores infos of a mangas as a common manga info for different manga sources, for further process
type Manga struct {
	Href       string                 `json:"href"`
	Name       string                 `json:"name"`
	Imghref    string                 `json:"imghref"`
	Status     MangaStatus            `json:"status"`
	Author     []string               `json:"author"`
	Newest     string                 `json:"newest"`
	UpdateTime time.Time              `json:"update_time"`
	Source     MangaSource            `json:"source"`
	ID         int                    `json:"id"`
	Infos      map[string]interface{} `json:"infos"` // Infos stores other nessasary infos
}

// New will stores new mangas to collector
func (c *Collector) New(href, name, imghref, newest string, id int, author []string, source MangaSource,
	infos map[string]interface{}) bool {
	defer c.Wait.Done()
	c.Loc.Lock()
	if _, ok := c.Indexer[name]; ok {
		c.Loc.Unlock()
		return false
	}
	c.Loc.Unlock()
	// Make sure that new insert success
	newm := &Manga{
		Href:    href,
		Name:    name,
		Imghref: imghref,
		Author:  author,
		Newest:  newest,
		Source:  source,
		ID:      id,
	}
	c.Loc.Lock()
	c.Indexer[name] = newm
	c.Loc.Unlock()
	return true
}

// Save will save collector to a file
func (c *Collector) Save(fname string) error {
	c.Wait.Wait()
	b, err := json.Marshal(c)
	if err != nil {
		return err
	}
	if err = ioutil.WriteFile(fname, b, os.ModePerm); err != nil {
		return err
	}
	return nil
}

// Load will load a collector
func Load(fname string) (*Collector, error) {
	b, err := ioutil.ReadFile(fname)
	if err != nil {
		return nil, err
	}
	c := &Collector{}
	if err = json.Unmarshal(b, c); err != nil {
		return nil, err
	}
	return c, nil
}
