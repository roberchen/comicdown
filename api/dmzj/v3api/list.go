package dmzj

import (
	"comicdown/collector"
	"strings"
	"time"
)

// Comic is manga info(in v3api)
type Comic struct {
	ID         int    `json:"id"`
	Title      string `json:"title"`
	Authors    string `json:"authors"`
	Cover      string `json:"cover"`
	Status     string `json:"status"`
	Types      string `json:"types"`
	LastUpdate int    `json:"last_updatetime"`
	Pop        int    `json:"num"`
}

// ToManga returns collector.Manga from Comic
func (c *Comic) ToManga() *collector.Manga {
	m := &collector.Manga{}
	m.Author = strings.Split(c.Authors, "/")
	m.Href = ""
	m.Imghref = c.Cover
	m.Name = c.Title
	m.Status = collector.MangaStatus(c.Status)
	m.UpdateTime = time.Unix(int64(c.LastUpdate), 0)
	m.Source = collector.Dmzj
	m.ID = c.ID
	m.Infos = map[string]interface{}{
		"Types":      c.Types,
		"Popularity": c.Pop,
	}

	return m
}
