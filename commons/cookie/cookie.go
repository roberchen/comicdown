// Package cookie implement a cookiejar which can save data to sqlite db,
// and be able to load cookie from firefox. It's the modification version
// of official Package cookiejar
package cookie

import (
	"errors"
	"fmt"
	"net"
	"net/http"
	"net/http/cookiejar"
	"net/url"
	"sort"
	"strings"
	"sync"
	"time"

	"github.com/jinzhu/gorm"
	// use for operating sqlite database
	_ "github.com/jinzhu/gorm/dialects/sqlite"
)

// Cookie is a cookie object from firefox
type Cookie struct {
	ID             uint   `gorm:"primary_key"`
	OrigAttr       string `gorm:"column:originAttributes;unique;not null;default:''"`
	Name           string `gorm:"column:name;unique"`
	Value          string `gorm:"column:value"`
	Host           string `gorm:"column:host;unique"`
	Path           string `gorm:"column:path;unique"`
	Expire         int64  `gorm:"column:expiry;type:integer"`
	ViewedAt       int64  `gorm:"column:lastAccessed;type:integer"`
	CreateAt       int64  `gorm:"column:creationTime;type:integer"`
	Security       int    `gorm:"column:isSecure;type:integer"`
	HTTPOnly       int    `gorm:"column:isHttpOnly;type:integer"`
	BrowserElement int    `gorm:"column:inBrowserElement;type:integer;default:0"`
	SameSite       int    `gorm:"column:sameSite;type:integer;default:0"`
	RawSameSite    int    `gorm:"column:rawSameSite;type:integer;default:0"`
	SchemeMap      int    `gorm:"column:schemeMap;type:integer;default:0"`

	// Persistent mark whether a cookie should be save to file
	Persistent bool `gorm:"-"`

	// seqNum is a sequence number so that Cookies returns cookies in a
	// deterministic order, even for cookies that have equal Path length and
	// equal Creation time. This simplifies testing.
	seqNum uint64
}

// TableName function, table name of cookie is moz_cookie
func (Cookie) TableName() string {
	return "moz_cookie"
}

// Jar implements the http.CookieJar interface from the net/http package.
type Jar struct {
	psList cookiejar.PublicSuffixList
	// mu locks the jar
	mu sync.Mutex
	// entries is a set of entries, keyed by their eTLD+1 and subkeyed by
	// their name/domain/path.
	entries map[string]map[string]*Cookie
	// nextSeqNum is the next sequence number assigned to a new cookie
	// created SetCookies.
	nextSeqNum uint64

	// file name of database file name
	dbfname string

	db *gorm.DB
}

// New returns a new cookie jar. A nil *Options is equivalent to a zero
// Options.
func New(o *cookiejar.Options, fname string) (j *Jar, err error) {
	j = initjar(o)
	j.db, err = gorm.Open("sqlite3", fname)
	if err != nil {
		return nil, err
	}
	j.db.CreateTable(&Cookie{})
	return j, nil
}

func initjar(o *cookiejar.Options) *Jar {
	jar := &Jar{
		entries: make(map[string]map[string]*Cookie),
	}
	if o != nil {
		jar.psList = o.PublicSuffixList
	}
	return jar
}

// Load will load a database
func Load(o *cookiejar.Options, fname string) (j *Jar, err error) {
	j = initjar(o)
	j.dbfname = fname
	j.db, err = gorm.Open("sqlite3", fname)
	if err != nil {
		return nil, err
	}
	if !j.db.HasTable(&Cookie{}) {
		return nil, fmt.Errorf("no moz_cookie table found in %s", fname)
	}
	// load all cookies from database
	var cookies []*Cookie
	j.db.Find(&cookies)
	for _, item := range cookies {
		key := jarKey(item.Host, j.psList)
		if _, ok := j.entries[key]; !ok {
			j.entries[key] = make(map[string]*Cookie)
		}
		j.entries[key][item.id()] = item
	}
	return j, nil
}

// Close will close cookie database
func (j *Jar) Close() (err error) {
	return j.db.Close()
}

// id returns the domain;path;name triple of c as an id.
func (c *Cookie) id() string {
	return fmt.Sprintf("%s;%s;%s", c.Host, c.Path, c.Name)
}

// shouldSend determines whether c's cookie qualifies to be included in a
// request to host/path. It is the caller's responsibility to check if the
// cookie is expired.
func (c *Cookie) shouldSend(https bool, host, path string) bool {
	return c.domainMatch(host) && c.pathMatch(path) && (https || c.Security == 0)
}

// domainMatch implements "domain-match" of RFC 6265 section 5.1.3.
func (c *Cookie) domainMatch(host string) bool {
	if c.Host == host {
		return true
	}
	return (c.HTTPOnly == 0) && hasDotSuffix(host, c.Host)
}

// pathMatch implements "path-match" according to RFC 6265 section 5.1.4.
func (c *Cookie) pathMatch(requestPath string) bool {
	if requestPath == c.Path {
		return true
	}
	if strings.HasPrefix(requestPath, c.Path) {
		if c.Path[len(c.Path)-1] == '/' {
			return true // The "/any/" matches "/any/path" case.
		} else if requestPath[len(c.Path)] == '/' {
			return true // The "/any" matches "/any/path" case.
		}
	}
	return false
}

// hasDotSuffix reports whether s ends in "."+suffix.
func hasDotSuffix(s, suffix string) bool {
	return len(s) > len(suffix) && s[len(s)-len(suffix)-1] == '.' && s[len(s)-len(suffix):] == suffix
}

// Cookies implements the Cookies method of the http.CookieJar interface.
//
// It returns an empty slice if the URL's scheme is not HTTP or HTTPS.
func (j *Jar) Cookies(u *url.URL) (cookies []*http.Cookie) {
	return j.cookies(u, time.Now())
}

// cookies is like Cookies but takes the current time as a parameter.
func (j *Jar) cookies(u *url.URL, now time.Time) (cookies []*http.Cookie) {
	if u.Scheme != "http" && u.Scheme != "https" {
		return cookies
	}
	host, err := canonicalHost(u.Host)
	if err != nil {
		return cookies
	}
	key := jarKey(host, j.psList)

	j.mu.Lock()
	defer j.mu.Unlock()

	submap := j.entries[key]
	if submap == nil {
		return cookies
	}

	https := u.Scheme == "https"
	path := u.Path
	if path == "" {
		path = "/"
	}

	modified := false
	var selected []*Cookie
	for id, c := range submap {
		// case: cookie has expired and is persistent: delete it
		if c.Persistent && c.Expire <= now.Unix() {
			delete(submap, id)
			modified = true
			// delete item from database
			j.db.Model(c).Delete(c)
			continue
		}
		if !c.shouldSend(https, host, path) {
			continue
		}
		c.ViewedAt = now.Unix()
		// update lastAccessed data
		j.db.Model(c).Update(*c)
		submap[id] = c
		selected = append(selected, c)
		modified = true
	}
	if modified {
		if len(submap) == 0 {
			delete(j.entries, key)
		} else {
			j.entries[key] = submap
		}
	}

	// sort according to RFC 6265 section 5.4 point 2: by longest
	// path and then by earliest creation time.
	sort.Slice(selected, func(i, j int) bool {
		s := selected
		if len(s[i].Path) != len(s[j].Path) {
			return len(s[i].Path) > len(s[j].Path)
		}
		if s[i].CreateAt != s[j].CreateAt {
			return s[i].CreateAt < s[j].CreateAt
		}
		return s[i].seqNum < s[j].seqNum
	})
	for _, e := range selected {
		cookies = append(cookies, &http.Cookie{Name: e.Name, Value: e.Value})
	}

	return cookies
}

// SetCookies implements the SetCookies method of the http.CookieJar interface.
//
// It does nothing if the URL's scheme is not HTTP or HTTPS.
func (j *Jar) SetCookies(u *url.URL, cookies []*http.Cookie) {
	j.setCookies(u, cookies, time.Now())
}

// setCookies is like SetCookies but takes the current time as parameter.
func (j *Jar) setCookies(u *url.URL, cookies []*http.Cookie, now time.Time) {
	if len(cookies) == 0 {
		return
	}
	if u.Scheme != "http" && u.Scheme != "https" {
		return
	}
	host, err := canonicalHost(u.Host)
	if err != nil {
		return
	}
	key := jarKey(host, j.psList)
	defPath := defaultPath(u.Path)

	j.mu.Lock()
	defer j.mu.Unlock()

	submap := j.entries[key]

	modified := false
	for _, cookie := range cookies {
		e, remove, err := j.newCookie(cookie, now, defPath, host)
		if err != nil {
			continue
		}
		id := e.id()
		if remove {
			if submap != nil {
				if _, ok := submap[id]; ok {
					delete(submap, id)
					modified = true
				}
			}
			j.db.Model(e).Delete(e)
			continue
		}
		if submap == nil {
			submap = make(map[string]*Cookie)
		}

		if old, ok := submap[id]; ok {
			e.CreateAt = old.CreateAt
			e.seqNum = old.seqNum
		} else {
			e.CreateAt = now.Unix()
			e.seqNum = j.nextSeqNum
			j.nextSeqNum++
		}
		e.ViewedAt = now.Unix()
		submap[id] = e
		modified = true
		j.db.Save(e)
	}

	if modified {
		if len(submap) == 0 {
			delete(j.entries, key)
		} else {
			j.entries[key] = submap
		}
	}

}

// newCookie creates an cookie from a http.Cookie c. now is the current time and
// is compared to c.Expires to determine deletion of c. defPath and host are the
// default-path and the canonical host name of the URL c was received from.
//
// remove records whether the jar should delete this cookie, as it has already
// expired with respect to now. In this case, e may be incomplete, but it will
// be valid to call e.id (which depends on e's Name, Domain and Path).
//
// A malformed c.Domain will result in an error.
func (j *Jar) newCookie(c *http.Cookie, now time.Time, defPath, host string) (e *Cookie, remove bool, err error) {
	e = &Cookie{}
	e.Name = c.Name

	if c.Path == "" || c.Path[0] != '/' {
		e.Path = defPath
	} else {
		e.Path = c.Path
	}

	e.Host, _, err = j.domainAndType(host, c.Domain)
	if err != nil {
		return e, false, err
	}

	// MaxAge takes precedence over Expires.
	if c.MaxAge < 0 {
		return e, true, nil
	} else if c.MaxAge > 0 {
		e.Expire = now.Add(time.Duration(c.MaxAge) * time.Second).Unix()
		e.Persistent = true
	} else {
		if c.Expires.IsZero() {
			e.Expire = endOfTime.Unix()
			e.Persistent = false
		} else {
			if !c.Expires.After(now) {
				return e, true, nil
			}
			e.Expire = c.Expires.Unix()
			e.Persistent = true
		}
	}

	e.Value = c.Value
	if c.Secure {
		e.Security = 1
	}
	if c.HttpOnly {
		e.HTTPOnly = 1
	}

	switch c.SameSite {
	case http.SameSiteDefaultMode:
		e.SameSite = 0
	case http.SameSiteStrictMode:
		e.SameSite = 1
	case http.SameSiteLaxMode:
		e.SameSite = 2
	}

	return e, false, nil
}

// canonicalHost strips port from host if present and returns the canonicalized
// host name.
func canonicalHost(host string) (string, error) {
	var err error
	host = strings.ToLower(host)
	if hasPort(host) {
		host, _, err = net.SplitHostPort(host)
		if err != nil {
			return "", err
		}
	}
	if strings.HasSuffix(host, ".") {
		// Strip trailing dot from fully qualified domain names.
		host = host[:len(host)-1]
	}
	return toASCII(host)
}

// hasPort reports whether host contains a port number. host may be a host
// name, an IPv4 or an IPv6 address.
func hasPort(host string) bool {
	colons := strings.Count(host, ":")
	if colons == 0 {
		return false
	}
	if colons == 1 {
		return true
	}
	return host[0] == '[' && strings.Contains(host, "]:")
}

// jarKey returns the key to use for a jar.
func jarKey(host string, psl cookiejar.PublicSuffixList) string {
	if isIP(host) {
		return host
	}

	var i int
	if psl == nil {
		i = strings.LastIndex(host, ".")
		if i <= 0 {
			return host
		}
	} else {
		suffix := psl.PublicSuffix(host)
		if suffix == host {
			return host
		}
		i = len(host) - len(suffix)
		if i <= 0 || host[i-1] != '.' {
			// The provided public suffix list psl is broken.
			// Storing cookies under host is a safe stopgap.
			return host
		}
		// Only len(suffix) is used to determine the jar key from
		// here on, so it is okay if psl.PublicSuffix("www.buggy.psl")
		// returns "com" as the jar key is generated from host.
	}
	prevDot := strings.LastIndex(host[:i-1], ".")
	return host[prevDot+1:]
}

// isIP reports whether host is an IP address.
func isIP(host string) bool {
	return net.ParseIP(host) != nil
}

// defaultPath returns the directory part of an URL's path according to
// RFC 6265 section 5.1.4.
func defaultPath(path string) string {
	if len(path) == 0 || path[0] != '/' {
		return "/" // Path is empty or malformed.
	}

	i := strings.LastIndex(path, "/") // Path starts with "/", so i != -1.
	if i == 0 {
		return "/" // Path has the form "/abc".
	}
	return path[:i] // Path is either of form "/abc/xyz" or "/abc/xyz/".
}

var (
	errIllegalDomain   = errors.New("cookiejar: illegal cookie domain attribute")
	errMalformedDomain = errors.New("cookiejar: malformed cookie domain attribute")
	errNoHostname      = errors.New("cookiejar: no host name available (IP only)")
)

// endOfTime is the time when session (non-persistent) cookies expire.
// This instant is representable in most date/time formats (not just
// Go's time.Time) and should be far enough in the future.
var endOfTime = time.Date(9999, 12, 31, 23, 59, 59, 0, time.UTC)

// domainAndType determines the cookie's domain and hostOnly attribute.
func (j *Jar) domainAndType(host, domain string) (string, bool, error) {
	if domain == "" {
		// No domain attribute in the SetCookie header indicates a
		// host cookie.
		return host, true, nil
	}

	if isIP(host) {
		// According to RFC 6265 domain-matching includes not being
		// an IP address.
		// TODO: This might be relaxed as in common browsers.
		return "", false, errNoHostname
	}

	// From here on: If the cookie is valid, it is a domain cookie (with
	// the one exception of a public suffix below).
	// See RFC 6265 section 5.2.3.
	if domain[0] == '.' {
		domain = domain[1:]
	}

	if len(domain) == 0 || domain[0] == '.' {
		// Received either "Domain=." or "Domain=..some.thing",
		// both are illegal.
		return "", false, errMalformedDomain
	}
	domain = strings.ToLower(domain)

	if domain[len(domain)-1] == '.' {
		// We received stuff like "Domain=www.example.com.".
		// Browsers do handle such stuff (actually differently) but
		// RFC 6265 seems to be clear here (e.g. section 4.1.2.3) in
		// requiring a reject.  4.1.2.3 is not normative, but
		// "Domain Matching" (5.1.3) and "Canonicalized Host Names"
		// (5.1.2) are.
		return "", false, errMalformedDomain
	}

	// See RFC 6265 section 5.3 #5.
	if j.psList != nil {
		if ps := j.psList.PublicSuffix(domain); ps != "" && !hasDotSuffix(domain, ps) {
			if host == domain {
				// This is the one exception in which a cookie
				// with a domain attribute is a host cookie.
				return host, true, nil
			}
			return "", false, errIllegalDomain
		}
	}

	// The domain must domain-match host: www.mycompany.com cannot
	// set cookies for .ourcompetitors.com.
	if host != domain && !hasDotSuffix(host, domain) {
		return "", false, errIllegalDomain
	}

	return domain, false, nil
}
