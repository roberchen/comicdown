package dmzj

import (
	"comicdown/api"
	"comicdown/collector"
	"comicdown/commons"
	"comicdown/commons/download"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"sync"
	"time"
)

type tags struct {
	ID   int          `json:"tag_id"`
	Name api.ListName `json:"title"`
}

// V3api is comic api provided by v3api.dmzj.com
type V3api struct {
	taglist map[string]tags
}

var ()

// GetListNames of v3api
func (v *V3api) GetListNames() ([]api.ListName, error) {
	if v.taglist != nil {
		out := make([]api.ListName, 10)
		for _, tag := range v.taglist {
			out = append(out, tag.Name)
		}

		return out, nil
	}
	log.Println("GetListNames: Start fetching list of v3api")
	v.taglist = make(map[string]tags)
	resp, err := http.Get("http://v3api.dmzj.com/0/category.json")
	if err != nil {
		return nil, err
	}
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	var list []tags
	if err := json.Unmarshal(data, &list); err != nil {
		return nil, err
	}
	for _, l := range list {
		v.taglist[string(l.Name)] = l
	}
	return v.GetListNames()
}

// getLists function, breakfunc returns whether collection should be break according to the scanning manga, false
// to continue fetching, true to break fetching
func (v *V3api) getList(ln api.ListName, breakfunc func(*collector.Manga) bool) ([]*collector.Manga, error) {
	// maybe the list should be save
	if v.taglist == nil {
		return nil, fmt.Errorf("comic lists nil, init it first")
	}
	tag, ok := v.taglist[string(ln)]
	if !ok {
		return nil, fmt.Errorf("list %s invalid", ln)
	}
	id := tag.ID
	out := make([]*collector.Manga, 0, 100)
	for i := 0; ; i++ {
		// newest of manga
		resp, err := http.Get(fmt.Sprintf("http://v3api.dmzj.com/classify/%d/1/%d.json", id, i))
		if err != nil {
			return nil, err
		}
		var list []Comic
		data, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return out, fmt.Errorf("body read failed: %s", err)
		}
		if err := json.Unmarshal(data, &list); err != nil {
			return out, fmt.Errorf("body Unmarshal failed: %s", err)
		}
		if len(list) == 0 {
			break
		}
		for _, c := range list {
			m := c.ToManga()
			out = append(out, m)
			if breakfunc(m) {
				break
			}
		}
	}

	// save the first comic info
	data, err := json.Marshal(out[0])
	if err == nil {
		if ok, err := commons.EnsurePath("dmzj/newest.json"); err != nil && ok {
			return out, err
		}
		err = ioutil.WriteFile(commons.GetPath("dmzj/newest.json"), data, os.ModePerm)
	}
	return out, err
}

// GetList returns the full manga lists of given listname
func (v *V3api) GetList(ln api.ListName) ([]*collector.Manga, error) {
	return v.getList(ln, func(*collector.Manga) bool {
		return false
	})
}

// UpdateList of v3api
func (v *V3api) UpdateList(ln api.ListName) ([]*collector.Manga, error) {
	data, err := ioutil.ReadFile(commons.GetPath("dmzj/newest.json"))
	if err == nil {
		return nil, fmt.Errorf("dmzj/newest.json not found, use GetList")
	}
	var newest *collector.Manga
	if err := json.Unmarshal(data, newest); err != nil {
		return nil, fmt.Errorf("cannot unmarshal newest manga info")
	}
	// fetch until meet prev newest

	return v.getList(ln, func(m *collector.Manga) bool {
		if m.Newest == newest.Newest &&
			strlistequal(m.Author, newest.Author) &&
			m.UpdateTime.Equal(newest.UpdateTime) {
			return true
		}
		return false
	})
}

func strlistequal(a []string, b []string) bool {
	if len(a) != len(b) {
		return false
	}
	for i, s := range a {
		if b[i] != s {
			return false
		}
	}
	return true
}

// GetInfo of v3api
func (*V3api) GetInfo(id int) (collector.Manga, error) {
	out := collector.Manga{}
	resp, err := http.Get(fmt.Sprintf("http://v3api.dmzj.com/comic/comic_%d.json", id))
	if err != nil {
		return out, err
	}
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return out, err
	}
	var m Manga
	if err = json.Unmarshal(data, &m); err != nil {
		return out, err
	}
	// Author
	for _, au := range m.Authors {
		out.Author = append(out.Author, au.Name)
	}
	out.Href = fmt.Sprintf("http://v3api.dmzj.com/comic/comic_%d.json", id)
	out.Imghref = m.Image
	out.Newest = m.Newest
	out.Source = collector.Dmzj
	out.Status = collector.MangaStatus(m.Status[0].Name)
	out.UpdateTime = time.Unix(int64(m.LastUpdate), 0)
	out.Name = m.Title
	out.ID = m.ID

	var infos []interface{}
	d, err := json.Marshal(m.Chapters)
	if err != nil {
		infos = nil
	}
	if err := json.Unmarshal(d, &infos); err != nil {
		infos = nil
	}
	out.Infos = map[string]interface{}{
		"chapters": infos,
	}
	return out, nil
}

// Fetch of v3api
func (*V3api) Fetch(id int) error {
	_, err := commons.EnsurePath(fmt.Sprintf("dmzj/download/%d/info.json", id))
	if err != nil {
		return err
	}
	// fetch infos of comic
	resp, err := http.Get(fmt.Sprintf("http://v3api.dmzj.com/comic/comic_%d.json", id))
	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("Fetch manga with id %d failed: cannot fetch info", id)
	}
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return fmt.Errorf("Fetch failed: cannot read info")
	}
	if _, err := commons.EnsurePath(fmt.Sprintf("dmzj/download/%d/info.json", id)); err != nil {
		return err
	}
	if err := ioutil.WriteFile(commons.GetPath(fmt.Sprintf("dmzj/download/%d/info.json", id)),
		data, os.ModePerm); err != nil {
		log.Printf("Fetch: write info.json for dmzj manga %d failed\n", id)
		return err
	}
	var info Manga
	if err := json.Unmarshal(data, &info); err != nil {
		return fmt.Errorf("Fetch failed: parse manga info failed")
	}

	// downloading content
	hdr := make(http.Header)
	hdr.Add("Host", "imgzip.dmzj.com")
	errch := make(chan error, 1)
	wg := new(sync.WaitGroup)
	for _, sec := range info.Chapters {
		wg.Add(len(sec.Data))
		go func(sec Section) {
			if ok, err := commons.EnsurePath(fmt.Sprintf("dmzj/download/%d/%s", id, sec.Title)); ok && err == nil {
				log.Printf("ERR: the path dmzj/download/%d/%s is file, not dir\n", id, sec.Title)
				errch <- err
				return
			} else if !ok && err != nil {
				log.Printf("ERR: cannot open path dmzj/download/%d/%s\n", id, sec.Title)
				errch <- err
				return
			} else {
				err := os.MkdirAll(commons.GetPath(fmt.Sprintf("dmzj/download/%d/%s", id, sec.Title)),
					os.ModeDir|os.ModePerm,
				)
				if err != nil {
					log.Printf("ERR: create path dmzj/download/%d/%s failed\n", id, sec.Title)
					errch <- err
					return
				}
			}
			// download sections
			for _, mg := range sec.Data {
				go func(mg Episode) {
					defer wg.Done()
					if ok, _ := commons.EnsurePath(
						fmt.Sprintf("dmzj/download/%d/%s/%d.zip",
							info.ID, sec.Title, mg.ID),
					); ok {
						// file exist
						return
					}
					req, err := http.NewRequest(http.MethodGet,
						fmt.Sprintf("http://images.dmzj.com/%s/%d/%d.zip", info.Firstletter,
							info.ID, mg.ID), nil)
					if err != nil {
						log.Printf("create request failed: %s", err)
						return
					}
					req.Host = "imgzip.dmzj.com"
					if err := download.Download(mg.Title,
						fmt.Sprintf("dmzj/download/%d/%s/%d.zip", info.ID, sec.Title, mg.ID),
						req,
					); err != nil {
						log.Printf("Fetch: downloading %d/%s/%d failed: %s\n",
							info.ID, sec.Title, mg.ID, err)
						return
					}
				}(mg)
			}
			errch <- nil
		}(sec)
	}
	wg.Wait()

	return <-errch
}
