package main

import (
	"comicdown/api/dmzj"
	"comicdown/collector"
	"fmt"

	"github.com/gocolly/colly/v2"
)

type iter struct {
	base string
	page int
}

func (i *iter) Next() string {
	defer func() {
		i.page++
	}()
	return fmt.Sprintf(i.base, i.page)
}

func (i *iter) IsEnd() bool { return false }

func main() {
	c := colly.NewCollector()
	collect := collector.NewCollector()
	i := &iter{
		base: "http://manhua.dmzj.com/tags/baihe/%d.shtml",
		page: 1,
	}
	c.Async = true
	dmzj.TagScan(c, collect, i)
	c.Wait()
	collect.Save("save.json")
}
