/*Package dmzj will resolve all scrap routing of dmzj
 */
package dmzj

import (
	"comicdown/collector"
	"comicdown/commons"
	"log"

	"github.com/gocolly/colly/v2"
)

// TagScan will init all route of colly.collector, the collector.Collector collects all manga infos.
func TagScan(c *colly.Collector, col *collector.Collector, urliter commons.URLIter) {
	c.OnHTML("div.tcaricature_new ul", func(e *colly.HTMLElement) {
		var href, name, img, newest string
		var author []string
		href = e.ChildAttr("li:first-child a", "href")
		name = e.ChildAttr("li:first-child a", "title")
		img = e.ChildAttr("li:first-child a img", "src")
		e.ForEach("li div.adiv2hidden", func(i int, h *colly.HTMLElement) {
			if i == 0 {
				author = h.ChildAttrs("a", "title")
			} else if i == 1 {
				newest = h.ChildAttr("a", "title")
			}
		})
		log.Printf("get %s name %s, author %s\n", href, name, author)
		col.Wait.Add(1)
		go col.New(href, name, img, newest, 0, author, collector.Dmzj, nil)
	})

	c.OnResponse(func(r *colly.Response) {
		if r.StatusCode != 200 {
			r.Request.Abort()
		} else {
			if !urliter.IsEnd() {
				c.Visit(urliter.Next())
			}
		}
	})

	c.OnRequest(func(r *colly.Request) {
		log.Printf("visiting %s\n", r.URL.String())
	})

	if !urliter.IsEnd() {
		c.Visit(urliter.Next())
	}
}
