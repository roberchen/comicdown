package commons

import (
	"fmt"
	"os"
	"path/filepath"
)

var (
	// dir must end with /
	dir = "."
)

// GetRootDir returns root working directory of comicdown, end with '/'.
func GetRootDir() string {
	return dir
}

// SetRootDir set the root working directory of comicdown
func SetRootDir(dirpath string) {
	if dirpath == "" {
		dir = "./"
	} else if dirpath[len(dirpath)-1] != '/' {
		dir = dirpath + "/"
	} else {
		dir = dirpath
	}
}

// GetPath returns proper path of given path
func GetPath(path string) string {
	if path == "" {
		return dir
	} else if path[0] != '/' {
		return dir + "/" + path
	} else {
		return dir + path
	}
}

// EnsurePath will ensure the path of file named path is useable, and check file existence.
// If the path exist, the function do nothing, if the path doesn't exit,
// the function will create the path.
// the path is reletive path based on root dir
func EnsurePath(path string) (bool, error) {
	fpath := filepath.Join(dir, path)
	dir, fname := filepath.Split(fpath)
	if err := os.MkdirAll(dir, os.ModeDir|os.ModePerm); err != nil {
		return false, err
	}
	fs, err := os.Stat(path)
	if err != nil && os.IsNotExist(err) {
		return false, nil
	}
	if fs.IsDir() {
		return true, fmt.Errorf("%s %s is a dir, not file", dir, fname)
	}
	return true, nil
}
