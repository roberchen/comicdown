package download

import (
	"comicdown/commons"
	"crypto/rand"
	"fmt"
	"log"
	"net/http"
	"os"
	"strconv"
	"sync"
	"testing"
	"time"

	"github.com/dustin/go-humanize"
)

func TestDownload(t *testing.T) {
	// server side
	http.HandleFunc("/testdownload", func(r http.ResponseWriter, q *http.Request) {
		// generate a rand file according to size and copy to response
		size, err := strconv.Atoi(q.URL.Query().Get("size"))
		log.Printf("Handling size :%d\t%s", size, humanize.Bytes(uint64(size)))
		r.Header().Add("Content-Length", strconv.Itoa(size))
		if err != nil {
			t.Error(err)
		}

		out := make([]byte, size, size)
		rand.Reader.Read(out)
		os, err := r.Write(out)
		if err != nil {
			t.Errorf("server write failed %s", err)
		}
		if os != size {
			t.Errorf("write size %d < %d", os, size)
		}
	})
	go func() {
		log.Println("Start server side")
		if err := http.ListenAndServe(":8888", nil); err != nil {
			t.Errorf("server start failed: %s", err)
		}
	}()
	time.Sleep(2 * time.Second)
	// creating download
	log.Println("Start client")
	m := NewManager()
	// set root
	commons.SetRootDir("./")
	var wg sync.WaitGroup
	for i := 1; i <= 10; i++ {
		wg.Add(1)
		go func(num int) {
			defer wg.Done()
			fname := fmt.Sprintf("%d.test", num)
			req, err := http.NewRequest(http.MethodGet,fmt.Sprintf("http://127.0.0.1:8888/testdownload?size=%d", 10*num<<20),nil )
			if err := m.Download("test", fname, req); err != nil {
				t.Errorf("download test failed: %s", err)
			}
			fi, err := os.Stat(fname)
			if err != nil {
				t.Errorf("open test download file %s failed, %s", fname, err)
				return
			}
			if fi.Size() != int64(10*num)<<20 {
				t.Errorf("size not same, expected %d, got %d", 10*num<<20, fi.Size())
			}
			os.Remove(fname)
		}(i)
	}
	wg.Wait()
}
