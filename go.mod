module comicdown

go 1.14

require (
	github.com/dustin/go-humanize v1.0.0
	github.com/go-sqlite/sqlite3 v0.0.0-20180313105335-53dd8e640ee7
	github.com/gocolly/colly/v2 v2.1.0
	github.com/gonuts/binary v0.2.0 // indirect
	github.com/jinzhu/gorm v1.9.16
	github.com/kless/term v0.0.0-20161130133337-e551c64f56c0 // indirect
	github.com/mitchellh/mapstructure v1.3.3
	github.com/sethgrid/curse v0.0.0-20181231162520-d4ee583ebf0f // indirect
	github.com/sethgrid/multibar v0.0.0-20160417171508-4bf4cf7b87d6
	github.com/tredoe/term v0.0.0-20161130133337-e551c64f56c0 // indirect
	github.com/vbauerster/mpb/v5 v5.3.0
	golang.org/x/net v0.0.0-20200602114024-627f9648deb9
	gorm.io/gorm v0.2.31
)
