package dmzj

// Manga is type to store dmzj manga info from v3api
type Manga struct {
	ID              int       `json:"id"`
	Islong          int       `json:"islong"`
	Title           string    `json:"title"`
	Image           string    `json:"cover"`
	Description     string    `json:"description"`
	LastUpdate      int       `json:"last_updatetime"`
	Newest          string    `json:"last_update_chapter_name"`
	NewestChapterID int       `json:"last_update_chapter_id"`
	Firstletter     string    `json:"first_letter"`
	Comicpy         string    `json:"comicpy"`
	Hidden          int       `json:"hidden"`
	Lock            int       `json:"is_lock"`
	Chapters        []Section `json:"chapters"`
	ChapterHidden   string       `json:"isHideChapter"`
	Authors         []Author  `json:"authors"`
	Status          []struct {
		ID   int    `json:"tag_id"`
		Name string `json:"tag_name"`
	} `json:"status"`
}

// Section is manga section of dmzj(in v3api)
type Section struct {
	Title string    `json:"title"`
	Data  []Episode `json:"data"`
}

// Author is manga author(in v3api)
type Author struct {
	ID   int    `json:"tag_id"`
	Name string `json:"tag_name"`
}

// Episode is a chapter of dmzj(in v3api)
type Episode struct {
	ID         int    `json:"chapter_id"`
	Title      string `json:"chapter_title"`
	Updatetime int    `json:"updatetime"`
	Filesize   int    `json:"filesize"`
	Order      int    `json:"chapter_order"`
}
