package cookie

import (
	"net/http"
	"net/http/cookiejar"
	"net/url"
	"testing"

	"golang.org/x/net/publicsuffix"
)

func TestCookieCreateLoad(t *testing.T) {
	var (
		name  = "cookie.sqlite.test"
		tests = []*http.Cookie{
			{Name: "abc", Value: "123"},
		}
		option = &cookiejar.Options{
			PublicSuffixList: publicsuffix.List,
		}
		testurl = &url.URL{Scheme: "https", Host: "bbs.yamibo.com"}
	)
	c, err := New(option, name)
	if err != nil {
		t.Errorf("New cookie database failed: %s ", err)
	}
	c.SetCookies(testurl, tests)
	if err := c.Close(); err != nil {
		t.Errorf("cookie database close failed: %s", err)
	}

	nc, err := Load(option, name)
	if err != nil {
		t.Errorf("Load cookie failed: %s", err)
	}

	clist := nc.Cookies(testurl)
	if clist[0].Domain != tests[0].Domain {
		t.Errorf("Domain is not %s, got %s", tests[0].Domain, clist[0].Domain)
	}
	if clist[0].Name != tests[0].Name {
		t.Errorf("Name is not %s, got %s", tests[0].Name, clist[0].Name)
	}
	if clist[0].Path != tests[0].Path {
		t.Errorf("Path is not %s, got %s", tests[0].Path, clist[0].Path)
	}
}
