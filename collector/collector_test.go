package collector

import (
	"os"
	"testing"
)

func TestLoadSave(t *testing.T) {
	c := NewCollector()
	tests := []*Manga{
		{Href: "/nsans/", Name: "女生爱女生", Imghref: "https://images.dmzj.com/webpic/18/glove.jpg", Author: []string{"桂游生丸"}, Newest: "第五卷", ID: 198},
		{Href: "/jbcm/", Name: "惊爆草莓", Imghref: "https://images.dmzj.com/webpic/4/jingbaocaomei20180810.jpg", Author: []string{"公野樱子"}, Newest: "第14话", ID: 424},
		{Href: "/plgs/", Name: "漂亮怪兽", Imghref: "https://images.dmzj.com/webpic/11/plgs.jpg", Author: []string{"七濑葵"}, Newest: "vol_10", ID: 471},
		{Href: "/jbcm/", Name: "惊爆草莓", Imghref: "https://images.dmzj.com/webpic/4/jingbaocaomei20180810.jpg", Author: []string{"公野樱子"}, Newest: "第14话", ID: 424},
	}

	for _, ele := range tests {
		c.Wait.Add(1)
		go c.New(ele.Href, ele.Name, ele.Imghref, ele.Newest, ele.ID, ele.Author, Dmzj, nil)
	}
	if err := c.Save("test.tmp"); err != nil {
		t.Errorf("Save file %s error: %s", "test.tmp", err)
	}
	nc, err := Load("test.tmp")
	if err != nil {
		t.Errorf("Load file %s error: %s", "test.tmp", err)
	}
	for _, ele := range tests {
		e, ok := nc.Indexer[ele.Name]
		if !ok {
			t.Errorf("element %s not found in new collector", ele.Name)
		}

		if e.Href != ele.Href {
			t.Errorf("element href not match: expected %s, get %s", ele.Href, e.Href)
		}
		if e.Name != ele.Name {
			t.Errorf("element name not match: expected %s, get %s", ele.Name, e.Name)
		}
		if e.Imghref != ele.Imghref {
			t.Errorf("element imghref not match: expected %s, get %s", ele.Imghref, e.Imghref)
		}
		if len(e.Author) != len(ele.Author) {
			t.Errorf("length of element author does not match, expected %d, get %d", len(ele.Author), len(e.Author))
		}
		for i := range e.Author {
			if e.Author[i] != ele.Author[i] {
				t.Errorf("element author not match: expected %s, get %s", ele.Author, e.Author)
			}
		}
		if e.Newest != ele.Newest {
			t.Errorf("element newest not match: expected %s, get %s", ele.Newest, e.Newest)
		}
	}
	_ = os.Remove("test.tmp")
}
