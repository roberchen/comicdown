/*Package commons contains some components
 */
package commons

// URLIter is a type which generates url
type URLIter interface {
	// Next returns next url
	Next() string
	// IsEnd tells whether the generator has stop
	IsEnd() bool
}
