// Package api wraps all online web-comic related behaviours
package api

import "comicdown/collector"

// ListName is name of comic list
type ListName string

// ComicPlatform is type which contains all comic platform actions
type ComicPlatform interface {
	GetList(listname ListName) ([]*collector.Manga, error)
	GetListNames() ([]ListName, error)
	UpdateList(listname ListName) ([]*collector.Manga, error)
	GetInfo(id int) (collector.Manga, error)
	Fetch(id int) error
}
