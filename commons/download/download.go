package download

import (
	"comicdown/commons"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"strconv"
	"sync"

	"github.com/dustin/go-humanize"
	"github.com/vbauerster/mpb/v5"
	"github.com/vbauerster/mpb/v5/decor"
)

// Manager is local download manager
type Manager struct {
	bars *mpb.Progress
	wg   *sync.WaitGroup
	cli  http.Client
}

var (
	defaultmng *Manager
)

func init() {
	defaultmng = NewManager()
}

// Download will download content of url to file path, use title as progress bar title, and hdr as http.Header
func Download(title, path string, req *http.Request) error {
	return defaultmng.Download(title, path, req)
}

// NewManager creates new download manager
func NewManager() *Manager {
	var wg sync.WaitGroup
	b := mpb.New(mpb.WithWaitGroup(&wg))
	return &Manager{
		bars: b,
		cli:  http.Client{},
		wg:   &wg,
	}
}

// Download will download content of url to file path, use title as progress bar title, and hdr as http.Header
//
// TODO: It's does not create new routine by now, will consider in future
func (m *Manager) Download(title, path string, req *http.Request) error {
	resp, err := m.cli.Do(req)
	if err != nil {
		return err
	}
	ctl := resp.Header.Get("Content-Length")
	var size int
	if ctl == "" {
		log.Printf("no content length")
	} else {
		size, err = strconv.Atoi(ctl)
		if err != nil {
			log.Printf("size unknown for %s", req.URL.String())
			size = 0
		}
	}
	bar := m.bars.AddBar(int64(size), mpb.BarStyle("╢▌▌░╟"),
		mpb.PrependDecorators(
			decor.Name(title+" "+path+" "+humanize.Bytes(uint64(size))),
			decor.Percentage(decor.WCSyncSpace),
		),
		mpb.AppendDecorators(
			// replace ETA decorator with "done" message, OnComplete event
			decor.OnComplete(
				// ETA decorator with ewma age of 60
				decor.EwmaETA(decor.ET_STYLE_GO, 60), "done",
			),
		),
	)
	newf, err := os.Create(commons.GetPath(path))
	defer newf.Close()
	if err != nil {
		return fmt.Errorf("cannot create file %s, %s", path, err)
	}
	for {
		s, err := io.CopyN(newf, resp.Body, 1024)
		if err != nil && err != io.EOF {
			return fmt.Errorf("download error: %s", err)
		}
		if s == 0 {
			break
		}
		bar.IncrInt64(s)
	}

	return nil
}

// Wait will wait all progress
func (m *Manager) Wait() {
	m.bars.Wait()
}

// Wait will wait for default download manager
func Wait() {
	defaultmng.Wait()
}
